package entities;
/*request*/
import org.junit.Assert;
import org.junit.Test;

public class comparePointsTest {
    @Test
    public void comparePoints() {

        Point o1 = new Point("Name1", 2, 0);
        Point o2 = new Point("Name2", 4, 1);
        int result = 0;

        if (o1.getX() == o2.getX() && o1.getY() < o2.getY()) {
            result = 0;
        } else if ((o1.getX() > o2.getX()) && (o1.getX() >= o2.getY())
                || o1.getY() > o2.getY() && o1.getY() >= o2.getX()) {
            result = 1;
        } else {
            result = -1;
        }

        Assert.assertEquals(-1, result);
    }

}
