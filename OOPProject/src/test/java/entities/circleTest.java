package entities;
/*request*/
import org.junit.Assert;
import org.junit.Test;


public class circleTest {

    @Test
    public void circleSquareTest() {
        int radius = 10;
        double result = Math.PI * Math.pow(radius, 2);
        Assert.assertEquals("Рассчет площади круга не корректен!", 314,(int)result);
    }

}
