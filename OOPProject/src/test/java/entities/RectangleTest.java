package entities;
/*request*/
import org.junit.Assert;
import org.junit.Test;

public class RectangleTest {

    @Test
    public void squareRectangleTest() {
        Point a = new Point("a", 0, 0);
        Point b = new Point("b", 4, 0);
        Point c = new Point("c", 4, 2);
        Point d = new Point("d", 0, 2);

        Rectangle r = new Rectangle(a, b, c, d);
        Assert.assertTrue(r.square() == 8.0);
    }

    @Test
    public void getNumberOfCornersTest() {
        Point[] arr = new Point[1];
        Point xPoint = new Point("test", 1, 1);
        arr[0] = xPoint;
        Assert.assertEquals("", true, arr != null);
        Assert.assertEquals("", true, arr[0] != null);
        Assert.assertEquals("", true, arr[0] == xPoint);
    }
}
