package entities;
/*request*/
public class Circle extends Figure {
    // Variable
    private double radius = 0;

    public Circle(Point center, double radius) {
        this.radius = radius;
        points = new Point[] { center };
    }

    // Getters and setters
    public Point getCenter() {
        return points[0];
    }

    public void setCenter(Point center) {
        points[0] = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    // Methods override
    @Override
    public double square() {
        return Math.PI* Math.pow(radius, 2);
    }

    @Override
    public String toString() {
        return "Окружность - [Центр круга =" + points[0] + ", радиус =" + radius + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Circle other = (Circle) obj;
        if (points[0] == null) {
            if (other.points[0] != null)
                return false;
        } else if (!points[0].equals(other.points[0]))
            return false;
        if (Double.doubleToLongBits(radius) != Double.doubleToLongBits(other.radius))
            return false;
        return true;
    }

}
