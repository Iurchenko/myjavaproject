package entities;
/*request*/
import java.util.Arrays;

public class Rectangle extends Polygon {

    private final int corners = 4;

    public Rectangle(Point...arr) {
        points = new Point[getNumberOfCorners()];
        points = arr;
        rectangleBuilder rc = new rectangleBuilder();

        if (!(rc.getRectangle(points))) {
            throw (new RuntimeException("Фигура не есть прямоугольником"));
        }
    }

    protected double getSideSize(Point x, Point y) {
        double sideSize = Math.sqrt(Math.pow((y.getX() - x.getX()), 2) + Math.pow((y.getY() - x.getY()), 2));
        return sideSize;
    }

    @Override
    public int getNumberOfCorners() {
        return corners;
    }

    @Override
    public double square() {
        return getSideSize(points[0], points[1]) * getSideSize(points[1], points[2]);
    }

    @Override
    public String toString() {
        return Arrays.toString(points);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + corners;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Rectangle other = (Rectangle) obj;
        if (corners != other.corners)
            return false;
        return true;
    }
}
