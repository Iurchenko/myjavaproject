package entities;
/*request*/
import java.util.Arrays;

public abstract class Figure {
    public Point[] points;

    abstract public double square();

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Figure other = (Figure) obj;
        if (!Arrays.equals(points, other.points))
            return false;
        return true;
    }
}
