package entities;
/*request*/
public abstract class Polygon extends Figure {
    public abstract int getNumberOfCorners();
}
