package entities;
/*request*/
import java.util.ArrayList;
import java.util.List;

public class rectangleBuilder {
    /**
     * Метод формирует коллекцию Vector(3 шт. Этого хватит для проверки), из которых
     * конструируется прямоугольник в том случае, если три из четырех углов = 90грд;
     * 
     * @param - Points - массив точек, из которых конструируем прямоугольник
     * @return -в случае верно заданных координат метод вернет TRUE, что посволяет
     *         зачислить фигуру в прямоугольники
     */
    private boolean create(Point[] Points) {
        boolean isGood = false;
        List<Vector> vectors = new ArrayList<Vector>();

        if (Points != null) {
            vectors.add(new Vector(Points[0], Points[1]));
            vectors.add(new Vector(Points[1], Points[2]));
            vectors.add(new Vector(Points[2], Points[3]));
            vectors.add(new Vector(Points[3], Points[0]));
        }

        for (int i = 0; i < vectors.size() - 1; i++) {
            if (angleChecker(vectors.get(i), vectors.get(i + 1)) && angleChecker(vectors.get(0), vectors.get(3)))
                isGood = true;
        }
        return isGood;
    }

    public boolean getRectangle(Point[] Points) {
        return create(Points);
    }

    /**
     * spv - скалярное произведение векторов modulVector - модуль вектора по формуле
     * >> https://ru.onlinemschool.com/math/library/vector/angl/ угол получаем в
     * радианах, далее конвертируем в градусы
     * 
     * @return в случае угла 90грд между векторами v1 и v2 метод вернет TRUE
     */
    private boolean angleChecker(Vector v1, Vector v2) {
        double spv = v1.getX() * v2.getX() + v1.getY() * v2.getY();
        double modulVector1 = Math.sqrt((Math.pow(v1.getX(), 2) + Math.pow(v1.getY(), 2)));
        double modulVector2 = Math.sqrt((Math.pow(v2.getX(), 2) + Math.pow(v2.getY(), 2)));
        double angle = Math.acos((spv / (modulVector1 * modulVector2)));

        double angleGradus = angle * 180 / Math.PI;

        if ((int) angleGradus == 90) {
            return true;
        }
        return false;
    }
}
