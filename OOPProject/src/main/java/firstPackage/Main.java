package firstPackage;
/*request*/
import entities.Circle;
import entities.Point;
import entities.Rectangle;

public class Main {

    public static void main(String[] args) {

        Point a = new Point("a", 0, 0);
        Point b = new Point("b", 1, 0);
        Point c = new Point("c", 1, 1);
        Point d = new Point("d", 0, 1);

        Rectangle r = new Rectangle(a, b, c, d);

        try {
            r.square();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        if (r.square() != 0)
            System.out.println("Площадь прямоугольника равна " + r.square());
        System.out.println("Количество углов равно " + r.getNumberOfCorners());

        Circle circ = new Circle(a, 7);
        circ.square();
        System.out.println("Площадь круга равна " + circ.square());
    }
}
