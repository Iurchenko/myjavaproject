package HomeWork11;

import java.util.Random;

public class ExerciseWithBigArrays {

    public static void main(String[] args) {

        Random rand = new Random();
        int rows = 5;
        int cols = 7;
        int[][] arr = new int[rows][cols];
        int sum = 0;

        // заполняем массив рандомными данными
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                arr[i][j] = rand.nextInt(10);
            }
        }

        // выводим на экран
        for (int i = 0; i < rows; i++) {
            System.out.println();
            for (int j = 0; j < cols; j++) {
                System.out.print(arr[i][j] + " ");
            }
        }

        // сумируем
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                sum += arr[i][j];
            }
        }

        // смотрим сумму на экране
        System.out.println("\nСумма всех элементов = " + sum);
    }
}
