package HomeWork10;

import java.util.Random;

public class Exercise11_245 {

    public static void main(String[] args) {
        // обьявляем массивы
        int[] initialArray = new int[15];
        int[] newArray = new int[15];
        Random random = new Random();

        // заполняем массив рандомными данными от -50 до 50
        for (int i = 0; i < initialArray.length; i++) {
            initialArray[i] = random.nextInt(100) - 50;
        }

        // перезаписываем данные в новый массив с нужными условиями
        int addedInBack = 0;
        int addedInBegin = 0;

        for (int i = 0; i < initialArray.length; i++) {
            if (initialArray[i] < 0) {
                newArray[addedInBegin] = initialArray[i]; // добавляем в начало и ведем учет кол-ва
                addedInBegin++;
            } else {
                newArray[newArray.length - 1 - addedInBack] = initialArray[i]; // добавляем в конец и ведем учет кол-ва
                addedInBack++;
            }
        }

        // выводим на монитор
        for (int i = 0; i < initialArray.length; i++) {
            System.out.println(newArray[i]);
        }
    }

}
