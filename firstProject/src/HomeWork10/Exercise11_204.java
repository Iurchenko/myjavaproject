package HomeWork10;
public class Exercise11_204 {

    public static void main(String[] args) {
        int[] arr = new int[] { 7, 4, 6, -5, -2, 0, -3, -1, 0, 0, 0 };
        int detectedIndexOne = 0;
        int detectedIndexTwo = 0;
        boolean isExist = false;

        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] % 2 != 0 && arr[i + 1] % 2 != 0) {
                detectedIndexOne = i;
                detectedIndexTwo = i + 1;
                isExist = true;
                break;
            }
        }
        
        if (!isExist) {
            System.err.println("Совпадений не найдено");
        } else
            System.out.printf("Индексы элементов первых соседних нечетных: %d и %d", detectedIndexOne,detectedIndexTwo);
    }
}
