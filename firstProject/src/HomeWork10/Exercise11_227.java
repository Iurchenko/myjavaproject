package HomeWork10;

import java.util.Random;

public class Exercise11_227 {

    public static void main(String[] args) {

        int[] temperatures = new int[30];
        int[] rainfall = new int[30];
        Random random = new Random();
        int summarryRain = 0;
        int summarySnow = 0;

        // заполняем оба массива рандомными данными
        for (int i = 0; i < temperatures.length; i++) {
            temperatures[i] = random.nextInt(50) - 20;
            rainfall[i] = random.nextInt(50);
        }

        // идем по массиву, копим снег и дождь отдельно
        for (int i = 0; i < temperatures.length; i++) {
            if (temperatures[i] > 0) {
                summarryRain += rainfall[i];
            } else {
                summarySnow += rainfall[i];
            }
        }

        System.out.println("Сумарно выпало снега " + summarySnow + " мм");
        System.out.println("Сумарно выпало дождя " + summarryRain + " мм");
    }
}
