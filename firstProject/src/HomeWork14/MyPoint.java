package HomeWork14;

import java.util.Random;

public class MyPoint {

    public static void main(String[] args) {
        /**
         * Точки с координатами Дан массив точек. Каждая точка характеризуется
         * координатами х, у и названием точки. Сортировать точки в массиве по
         * возрастанию удаления расстояния точки от начала координат и вывести их на
         * экран
         */
        // создаем переменные и заполняем массив
        Random rand = new Random();
        Point[] pointArray = new Point[4];

        Point a = new Point("a",rand.nextInt(10),rand.nextInt(10));
        pointArray[0] = a;

        Point b = new Point("b",rand.nextInt(10),rand.nextInt(10));
        pointArray[1] = b;

        Point c = new Point("c",rand.nextInt(10),rand.nextInt(10));
        pointArray[2] = c;

        Point d = new Point("d",rand.nextInt(10),rand.nextInt(10));
        pointArray[3] = d;

        showArray(sort(pointArray));

    }

    public static void showArray(Point[] pointArray) {
        for (int i = 0; i < pointArray.length; i++)
            System.out.printf("Name = %S;\n x = %d;\n y = %d;\n\n", pointArray[i].name, pointArray[i].x,
                    pointArray[i].y);
    }

    /**
     * Метод имеет две переменные типа double, в которые мы записываем расстояние
     * между точками и началом координат по формуле AB = √(xb - xa)2 + (yb - ya)2.
     * Далее сравниваем эти переменные и в зависимости от результата сортируем
     * пузырьком Возвращаем копию массива
     * 
     * @param pointArray - массив точек
     */
    private static Point[] sort(Point[] pointArray) {
        double firstDistance, secondDistance;
        for (int i = pointArray.length - 1; i > 0; --i) {
            for (int j = 0; j < i; j++) {
                firstDistance = Math.sqrt(Math.pow((pointArray[j].x - 0), 2) + Math.pow((pointArray[j].y - 0), 2));
                secondDistance = Math.sqrt(Math.pow((pointArray[j + 1].x - 0), 2) + Math.pow((pointArray[j + 1].y - 0), 2));
                if (firstDistance > secondDistance) {
                    Point tmp = pointArray[j];
                    pointArray[j] = pointArray[j + 1];
                    pointArray[j + 1] = tmp;
                }
            }
        }
        Point[] sortedPointArray = new Point[4];
        System.arraycopy(pointArray, 0, sortedPointArray, 0, pointArray.length);

        return sortedPointArray;
    }

}

class Point {
    String name = "";
    int x = 0;
    int y = 0;

    public Point(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }
}
