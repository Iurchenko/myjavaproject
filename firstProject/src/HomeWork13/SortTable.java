package HomeWork13;

import java.util.Random;
import java.util.Scanner;

public class SortTable {

    public static void main(String[] args) {

        /**
         * Дана таблица с числами. Отсортировать таблицу по заданному номеру столбца.
         */
        // объявляем переменные
        int rows = 5;
        int cols = 10;
        int[][] table = new int[rows][cols];
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите номер столбца для сортировки в пределах от 0 до 9");
        int colForSort = scan.nextInt();

        addRandomValueToArray(table); // заполняем рандомными данными
        System.out.println("Неотсортированая таблица");
        show(table); // показываем неотсортированную таблицу

        sort(table, colForSort); // сортируем таблицу по столбцу colForSort
        System.out.println("\nОтсортировано по номеру столбца : " + colForSort);
        show(table); // показываем отсортированную таблицу

        scan.close();
    }

    /**
     * 
     * @param table      - таблица для сортировки
     * @param colForSort - индекс столбца для сортировки
     * @param startIndex - индекс строки для синхронизации всех остальных данных,
     *                   после сортировки по столбцу colForSort. Будут
     *                   синхронизированы строки startIndex и startIndex+1
     */
    public static void synchronizeSwap(int[][] table, int colForSort, int startIndex) {
        for (int i = 0; i < table[0].length; i++) {
            if (i != colForSort) {
                int tmp = table[startIndex + 1][i];
                table[startIndex + 1][i] = table[startIndex][i];
                table[startIndex][i] = tmp;
            }
        }
    }

    /**
     * @param table      - таблица для сортировки
     * @param colForSort - индекс столбца для сортировки
     * @return - отсортированная таблица
     */
    public static void sort(int[][] table, int colForSort) {
        for (int i = table.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (table[j + 1][colForSort] < table[j][colForSort]) {
                    int tmp = table[j + 1][colForSort];
                    table[j + 1][colForSort] = table[j][colForSort];
                    table[j][colForSort] = tmp;

                    synchronizeSwap(table, colForSort, j);
                }
            }
        }
    }

    public static void addRandomValueToArray(int[][] table) {
        Random rand = new Random();
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[0].length; j++) {
                table[i][j] = rand.nextInt(30) + 10;
            }
        }
    }

    public static void show(int[][] table) {
        for (int i = 0; i < table.length; i++) {
            System.out.println();
            for (int j = 0; j < table[0].length; j++) {
                System.out.print(table[i][j]);
                System.out.print(' ');
            }
        }
        System.out.println();
    }
}
