package HomeWork13;

import java.util.Scanner;

public class Recursion {

    public static void main(String[] args) {
        /**
         * 10.53. Написать рекурсивную процедуру для ввода с клавиатуры
         * последовательности чисел и вывода ее на экран в обратном порядке (окончание
         * последовательности — при вводе нуля).
         */

        showDigit();
    }

    public static int showDigit() {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if (n > 0) {
            showDigit();
        }
        if (n == 0) {
            scan.close();
            return 0;
        }
        System.out.printf("%d ", n);
        scan.close();
        return 0;
    }
}
