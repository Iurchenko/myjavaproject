package HomeWork4;

import java.util.Scanner;

public class Exercise240 {

	public static void main(String[] args) {
		/*
		 * � ������ ����� ������� ������� ����������� �� y �������� (0 <= y < 360, y �
		 * ������������ �����). ���������� ����� ������ ����� � ����� ������ �����,
		 * ��������� � ������ �����.
		 */
		Scanner scan = new Scanner(System.in);

		System.out.println("�� ������� �������� ����������� �������?");
		double angle = scan.nextDouble(); // ���� �������� � ������ �����
		angle %= 360; // �������� ���� ����� ������ ��� 360 ��������

		int secondInHalfDay = 86400 / 2; // ����������� ������ � �������� �����
		int units = secondInHalfDay / 360; // ������ �������� 1 ������ = X ������

		long hours = (long) (angle * units / 60 / 60); // �������� ���-�� �����
		long minutes = (long) (angle * units / 60) % 60; // �������� ������ � �������� ������ ����

		System.out.println("������� ����������� �� " + angle + " ��������.");
		System.out.println("� ������ ����� ������ " + hours + " ����� � " + minutes + " �����.");

		scan.close();
	}

}
