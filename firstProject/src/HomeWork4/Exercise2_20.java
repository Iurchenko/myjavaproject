package HomeWork4;

import java.util.Scanner;

public class Exercise2_20 {

    public static void main(String[] args) {

        /*
         * �) �����, ���������� ��� ��������� ��� ���� ������ ������;
         * 
         * �) �����, ���������� ��� ������������ ������ � ������, ������� � ���������
         * ���� ��������� �����. ��������, �� ����� 5434 �������� 4543, �� ����� 7048�
         * 784;
         * 
         * �) �����, ���������� ��� ������������ ������ � ������� ���� ��������� �����.
         * ��������, �� ����� 5084 �������� 5804;
         * 
         * �) �����, ���������� ��� ������������ ���� ������ � ���� ��������� ����
         * ��������� �����. ��������, �� ����� 4566 �������� 6645, �� ����� 7304 � 473.
         * ��������� ������ ������ ����� ���������: 
         *      1) ��� ��������� ��������� ���� ��������� �����.
         *      2) � ���������� ��������� ���� ��������� �����; 
         */

        System.out.println("������� �������������� �����...");
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        
        // ��������� �� ��������� �����
        int edenici = num % 10 / 1; // ������� ��������
        int desjatki = num % 100 / 10; // ������� �������
        int sotni = num % 1000 / 100; // ������� �������
        int tisjachi = num % 10000 / 1000; // ������� ������
        
        //������������, ��� ��� ����
        int resultA = edenici * 1000 + desjatki * 100 + sotni * 10 + tisjachi * 1;
        int resultB = edenici * 10 + desjatki * 1 + sotni * 1000 + tisjachi * 100;
        int resultC = edenici * 1 + desjatki * 100 + sotni * 10 + tisjachi * 1000;
        
        // ������� '�' � ����������� ���� ��������� �����
        int resultD = edenici * 100 + desjatki * 1000 + sotni * 1 + tisjachi * 10;

        // ������� '�' ��� ��������� ���� ��������� �����
        int tmpValue1 = num / 100; // �������� ������ ��� �����
        int tmpValue2 = num % 100; // �������� ��������� ��� �����
        int resultE = tmpValue1 + tmpValue2 * 100;

        System.out.println("���� �����: " + num);
        System.out.println("��������� -�-: " + resultA);
        System.out.println("��������� -�-: " + resultB);
        System.out.println("��������� -�-: " + resultC);
        System.out.println("��������� -�- � ����������� �� ��������� �����: " + resultD);
        System.out.println("��������� -�- ��� ��������� ���� ��������� �����-: " + resultE);

        scan.close();

    }

}
