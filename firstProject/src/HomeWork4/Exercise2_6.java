package HomeWork4;

import java.util.Scanner;

public class Exercise2_6 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("������� ������? �?");

        int sec = scan.nextInt();
        int secondInDay = 24 * 60 * 60;
        int minInDay = 24 * 60;
        int hourInDay = 24;

        int hours = (sec / 60 / 60) % hourInDay; // ������� ��������� � ���� % ����� � ������
        int min = (sec / 60 % minInDay) % 60;    // ������� ��������� � ������ % ����� � ������ % �������� ������ ����
        int second = ((sec % secondInDay) % 60); // ������� % ������ � ������ % �������� ������ ������

        System.out.println("���� " + hours);
        System.out.println("������ " + min);
        System.out.println("������ " + second);

        scan.close();

    }

}
