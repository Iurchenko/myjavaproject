package HomeWork25;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PointStringParser {

    public static void main(String[] args) {

        String str = "Point {y:15,x:40,name:Upper Left}";
        Point testPoint = new Point();
        System.out.println(findKey(str, testPoint));

    }

    public static Point findKey(String s, Point point) {
        String patternY = "(y:\\d+)";
        String patternX = "(x:\\d+)";
        String patternValue = "\\d+";
        String patternName = "name(:\\w+.*\\w+)";
        String patternValueName = "([^name:]).*";

        point.setX(Integer.parseInt(find(s, patternX, patternValue)));
        point.setY(Integer.parseInt(find(s, patternY, patternValue)));
        point.setName(find(s, patternName, patternValueName));

        return point;
    }

    private static String find(String s, String pattern, String patternValue) {
        String result = "";
        String value = "";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(s);      
        if (m.find())
            result = m.group(0).toString();
        r = Pattern.compile(patternValue);
        m = r.matcher(result);
        if (m.find())
            value = m.group(0);
        return value;
    }
}
