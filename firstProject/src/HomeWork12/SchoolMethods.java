package HomeWork12;

import java.util.Random;

public class SchoolMethods {

    public static void main(String[] args) {
        int classes = 4;
        int parallels = 11;
        int[][] children = new int[parallels][classes];

        fillValueClasses(children);

        // Решение с дополнительным массивом
        System.out.println("Результат №1 - с дополнительным массивом (Методы и переменные Ver1.)");
        int bigParallel = findBigParralelsVer1(children);
        int smallParallel = findSmallParralelsVer1(children);
        show(bigParallel, smallParallel);

        // Решение без дополнительного массива
        System.out.println("Результат №2 - без дополнительного массива (Методы и переменные Ver2.)");
        int bigParallel2 = findBigParallelsVer2(children);
        int smallParallel2 = findSmallParralelsVer2(children);
        show(bigParallel2, smallParallel2);

        showAllParallels(children);// Вывод массива и сумм параллелей для проверки

    }

    public static int findBigParralelsVer1(int[][] array) {
        int[] parallelArray = new int[array.length];
        int tmp = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                parallelArray[i] += array[i][j];
            }
        }
        for (int i = 0; i < array.length - 1; i++) {
            if (tmp < parallelArray[i]) {
                tmp = parallelArray[i];
            }
        }
        return tmp;
    }

    public static int findBigParallelsVer2(int[][] array) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            int sum = 0;
            for (int j = 0; j < array[0].length; j++) {
                sum += array[i][j];
            }
            if (max < sum)
                max = sum;
        }
        return max;
    }

    public static int findSmallParralelsVer1(int[][] array) {

        int[] sumParallel = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                sumParallel[i] += array[i][j];
            }
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length - 1; i++) {
            if (min > sumParallel[i]) {
                min = sumParallel[i];
            }
        }
        return min;
    }

    public static int findSmallParralelsVer2(int[][] array) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            int sum = 0;
            for (int j = 0; j < array[0].length; j++) {
                sum += array[i][j];
            }
            if (min > sum)
                min = sum;
        }
        return min;
    }

    public static void fillValueClasses(int[][] array) {
        Random rand = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = rand.nextInt(10);
            }
        }
    }

    public static void show(int max, int min) {
        System.out.print("Численность самой большой параллели = " + max + "; ");
        System.out.println("Численность самой маленькой параллели = " + min + "; \n");
    }

    public static void showAllParallels(int[][] array) {
        System.out.println("Вывод массива и сумм параллелей для проверки");
        for (int i = 0; i < array.length; i++) {
            int sum = 0;
            System.out.print("\n");
            for (int j = 0; j < array[0].length; j++) {
                sum += array[i][j];
                System.out.print(array[i][j] + " ");
            }
            System.out.print(" " + sum);
        }
    }
}
