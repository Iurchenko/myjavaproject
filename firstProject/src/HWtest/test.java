package HWtest;

public class test {
    public static void main(String[] args) {
        int[] data = { 1, 3, 5, 8, 10, 15, 18, 25 };
        final int element = 1;

        System.out.println(binarySearch(data, element));
    }

    public static boolean binarySearch(int[] data, int element) {
        return binarySearch(data, element, 0, data.length - 1);
    }

    private static boolean binarySearch(int[] data, int element,
            int from, int to) {
        int middle;
        if (from == to) {
            return data[0] == element;
        }

        middle = (to-from) / 2;
        if (data[middle] == element) {
            return true;
        }if (element<data[middle]) {
            to = middle;
            return binarySearch(data, element);

        } if (element > data[middle] ) 
        {from = middle;
        return binarySearch(data, element);

        }
        return false;
    } 
}

