package HomeWork26;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ListTests {
    static Random rd = new Random();

    public static void main(String[] args) {
        /*
         * 
         * 
         * Протестировать и сравнить производительность ArrayList и LinkedList для
         * коллекции на 100_000 элементов. Протестировать вставки в середину, начало,
         * конец. Удаление из середины, начала, конца. Сортировку 2х коллекций (через
         * Collections.sort())
         * 
         * ВЫВОДЫ: 
         * 
         * - ArrayList хорош при добавлении в начало, средину и конец. Хорош при
         * удалении с конца. Сортируется быстрее, чем Linked
         * 
         * - LinkedList очень хорош придобавлении в начало, плох при добавлении в
         * средину и в конец. Хорош при удалении с начала и с конца. Сортируется
         * медленно.
         */

        List<Integer> testList = new ArrayList<Integer>();
        List<Integer> testLinkedList = new LinkedList<>();

        System.out.println("Тест добавления ArrayList на 100_000 елементов.");
        addToBegin(testList);
        addToMiddle(testList);
        addToEnd(testList);
        System.out.println();

        System.out.println("Тест удаления ArrayList на 100_000 елементов.");
        removeToBegin(testList);
        removeToMiddle(testList);
        removeToEnd(testList);
        System.out.println();

        System.out.println("Тест добавления LinkedList на 100_000 елементов.");
        addToBegin(testLinkedList);
        addToMiddle(testLinkedList);
        addToEnd(testLinkedList);
        System.out.println();

        System.out.println("Тест удаления LinkedList на 100_000 елементов.");
        removeToBegin(testLinkedList);
        removeToMiddle(testLinkedList);
        removeToEnd(testLinkedList);
        System.out.println();

        System.out.println();
        System.out.println("Создаем 2 тестовых List...");
        List<Integer> testSortArrayList = new ArrayList<Integer>();
        addToBegin(testSortArrayList);

        List<Integer> testSortLinkedList = new LinkedList<>();
        addToBegin(testSortLinkedList);
        System.out.println();

        System.out.println("Сортировка ArrayList на 100_000 елементов.");
        sortLinkedList(testSortArrayList);
        System.out.println();

        System.out.println("Сортировка LinkedList на 100_000 елементов.");
        sortLinkedList(testSortLinkedList);
        System.out.println();

    }

    public static void addToBegin(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            lst.add(0, new Integer(rd.nextInt()));
        }
        long stop = System.currentTimeMillis();
        System.out.println("Stop add 100_000 elements to begin position: " + (stop - start) + " miliseconds.");
    }

    public static void addToMiddle(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            lst.add(50_000, new Integer(rd.nextInt()));
        }
        long stop = System.currentTimeMillis();
        System.out.println("Stop add 100_000 elements to middle position: " + (stop - start) + " miliseconds.");
    }

    public static void addToEnd(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            lst.add(100_000, new Integer(rd.nextInt()));
        }
        long stop = System.currentTimeMillis();
        System.out.println("Stop add 100_000 elements to end position: " + (stop - start) + " miliseconds.");
    }

    public static void removeToBegin(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            lst.remove(0);
        }
        long stop = System.currentTimeMillis();
        System.out.println("Stop remove 100_000 elements from begin position: " + (stop - start) + " miliseconds.");
    }

    public static void removeToMiddle(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            lst.remove(lst.size() / 2);
        }
        long stop = System.currentTimeMillis();
        System.out.println("Stop remove 100_000 elements from middle position: " + (stop - start) + " miliseconds.");
    }

    public static void removeToEnd(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100_000; i++) {
            lst.remove(lst.size() - 1);
        }
        long stop = System.currentTimeMillis();
        System.out.println("Stop remove 100_000 elements from end position: " + (stop - start) + " miliseconds.");
    }

    public static void sortArrayList(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < lst.size(); i++) {
            Collections.sort(lst);
        }
        long stop = System.currentTimeMillis();
        System.out.println("Sorting: " + (stop - start) + " miliseconds.");
    }

    public static void sortLinkedList(List<Integer> lst) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < lst.size(); i++) {
            Collections.sort(lst);
        }
        long stop = System.currentTimeMillis();
        System.out.println("Sorting: " + (stop - start) + " miliseconds.");
    }
}
