package HomeWork3;

public class Exercise117 {

	public static void main(String[] args) {
		// Method
		Exe8();
	}

	// ������� 117 �
	static void Exe1() {
		int x1 = 7;
		int x2 = 7;
		System.out.println("���������: " + Math.sqrt(Math.pow(x1, 2) + (Math.pow(x2, 2))));
	}

	// ������� 117 �
	static void Exe2() {
		int x1 = 5;
		int x2 = 2;
		int x3 = 3;
		System.out.println("���������: " + ((x1 * x2) + (x1 * x3) + (x2 * x3)));
	}

	// ������� 117 �
	static void Exe3() {
		int v0 = 5;
		int t = 1;
		int a = 1;
		System.out.println("���������: " + (v0 * t + ((double) a * Math.pow(t, 2) / 2)));
	}

	// ������� 117 �
	static void Exe4() {
		int m = 5;
		double v = 2.8;
		int h = 3;
		final double g = 9.8;
		System.out.println("���������: " + (((double) m * Math.pow(v, 2)) / 2 + m * g * h));
	}

	// ������� 117 �
	static void Exe5() {
		final double R1 = 3.7;
		final double R2 = 2.0;
		System.out.println("���������: " + (1 / (double) R1 + 1 / (double) R2));
	}

	// ������� 117 �
	static void Exe6() {
		int m = 3;
		final double g = 9.8;
		int alpha = 54; // radian
		System.out.println("���������: " + (m * g * Math.cos(alpha)));
	}

	// ������� 117 �
	static void Exe7() {
		final double R = 3.7;
		final double Pi = Math.PI;
		System.out.println("���������: " + (2 * Pi * R));
	}

	// ������� 117 �
	static void Exe8() {
		double a = 6;
		double b = 3.9;
		double c = 6;
		System.out.println("���������: " + (Math.pow(b, 2) - 4 * a * c));
	}

	// ������� 117 �
	static void Exe9() {
		int m1 = 2;
		int m2 = 2;
		int r = 7;
		int y = 3;
		System.out.println("���������: " + (y * (((double) m1 * m2) / (Math.pow(r, 2)))));
	}

	// ������� 117 �
	static void Exe10() {
		final double I = 2;
		final double R = 2;
		System.out.println("���������: " + (Math.pow(I, 2) * R));
	}

	// ������� 117 �
	static void Exe11() {
		int a = 44;
		int b = 23;
		double c = 69;// radian
		System.out.println("���������: " + (a * b * Math.sin(c)));
	}

	// ������� 117 �
	static void Exe12() {
		int a = 14;
		int b = 23;
		double c = 129;// radian
		System.out.println("���������: " + Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2) - 2 * a * b * Math.cos(c)));
	}

	// ������� 117 �
	static void Exe13() {
		int a = 2;
		int b = 2;
		int c = 9;
		int d = 2;
		System.out.println("���������: " + ((((double) a * d) + (b * c)) / (a * d)));
	}

	// ������� 117 �
	static void Exe14() {
		int x = 2;// radian
		System.out.println("���������: " + Math.sqrt(1 - Math.sin(Math.pow(x, 2))));
	}

	// ������� 117 �
	static void Exe15() {
		int a = 4;
		int b = 5;
		int c = 6;
		int x = 2;
		System.out.println("���������: " + (1 / (Math.sqrt((a * Math.pow(x, 2)) + (b * x) + c))));
	}

	// ������� 117 �
	static void Exe16() {
		int x = 1;
		System.out.println("���������: " + (((Math.sqrt(x + 1)) + (Math.sqrt(x - 1))) / (2 * Math.sqrt(x))));
	}

	// ������� 117 �
	static void Exe17() {
		int x = -64;
		System.out.println("���������: " + (Math.abs(x) + Math.abs(x + 1)));
	}

	// ������� 117 �
	static void Exe18() {
		int x = -74;
		System.out.println("���������: " + (Math.abs((1 - Math.abs(x)))));
	}

}
