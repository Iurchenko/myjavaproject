﻿package HomeWork9;

public class Exercise11_136 {

	public static void main(String[] args) {
/*
		В массиве хранится информация о годе рождения каждого из 30 человек.
		Определить года рождения двух самых старших по возрасту людей. При
		определении возраста учитывать только год рождения (месяц и день не
		учитывать).
		Примечание
		Задачу решить, не используя два прохода по массиву.*/
		
		// создаем переменные
		int[] peoplesYearOfBirth = new int[30];

		// формируем массив, заполняем рандомным числом 1995 - 1950г.
		for (int i = 0; i < peoplesYearOfBirth.length; i++)
			peoplesYearOfBirth[i] = 1950 + (int) (Math.random() * ((1995 - 1950) + 1));

		// сортируем массив
		for (int i = 0; i < peoplesYearOfBirth.length; i++) {
			for (int j = 0; j < peoplesYearOfBirth.length; j++) {
				if (peoplesYearOfBirth[i] > peoplesYearOfBirth[j]) {
					int tmp = peoplesYearOfBirth[i];
					peoplesYearOfBirth[i] = peoplesYearOfBirth[j];
					peoplesYearOfBirth[j] = tmp;
				}
			}
		}

		// смотрим массив на екране
		for (int i = 0; i < peoplesYearOfBirth.length; i++) {
			System.out.println(peoplesYearOfBirth[i]);
		}

		// смотрим двоих старших на екране
		System.out.printf("Года рождения старших из них %d, %d", peoplesYearOfBirth[peoplesYearOfBirth.length-2], peoplesYearOfBirth[peoplesYearOfBirth.length-1]);
	}

}
