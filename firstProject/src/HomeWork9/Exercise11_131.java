﻿package HomeWork9;

import java.util.Random;

public class Exercise11_131 {

	public static void main(String[] args) {
		
		/*При выборе места строительства жилого комплекса при металлургическом
		комбинате необходимо учитывать "розу ветров" (следует расположить жи-
		лой комплекс так, чтобы частота ветра со стороны металлургического ком-
		бината была бы минимальной). Для этого в течение года проводилась реги-
		страция направления ветра в районе строительства. Данные представлены
		в виде массива, в котором направление ветра за каждый день кодируется
		следующим образом: 1 — северный, 2 — южный, 3 — восточный, 4 — за-
		падный, 5 — северо-западный, 6 — северо-восточный, 7 — юго-западный,
		8 — юго-восточный. Определить, как должен быть расположен жилой ком-
		плекс по отношению к комбинату.
		*/
		
		
		// создаем переменные
		int[] windVectorsMeasurement = new int[365];
		int minimalElement = Integer.MAX_VALUE;

		int northern = 0;
		int south = 0;
		int west = 0;
		int east = 0;
		int northwestern = 0;
		int northeastern = 0;
		int southeastern = 0;
		int southwestern = 0;

		// заполняем массив направлений вручную 
		int[] windType = new int[] { northern, south, west, east, northwestern, northeastern, southeastern, southwestern };
		
		//создаем обьект рандома
		Random random = new Random();
		
		// заполняем рандомным направлением за год
		for (int i = 0; i < windVectorsMeasurement.length; i++) {
			windVectorsMeasurement[i] = random.nextInt(8);
		}

		// вычисляем самое частое направление ветра
		for (int i = 0; i < windVectorsMeasurement.length; i++) {
			windType[windVectorsMeasurement[i]]++;
		}

		//находим минимально употребляемое направление ветра
		int counter = 0;
		for (int i = 0; i < windType.length; i++) {
			if (minimalElement > windType[i]) {
				minimalElement = windType[i];
				counter = i;
			}
		}
		
		//проверяем на повтор минимального направления
		int counter2 = -1;
		for (int i = 0; i < windType.length; i++) {
			if (i != counter) {
				if (windType[counter] == windType[i])
					counter2 = i;
			}
		}

		// получаем нужное направление для строительства, если оно единственное
		switch (counter) {
		case 0:
			System.out.print("you can build in the northern");
			break;

		case 1:
			System.out.print("you can build in the south");
			break;

		case 2:
			System.out.print("you can build in the west");
			break;

		case 3:
			System.out.print("you can build in the east");
			break;

		case 4:
			System.out.print("you can build in the northwestern");
			break;

		case 5:
			System.out.print("you can build in the northeastern");
			break;

		case 6:
			System.out.print("you can build in the southeastern");
			break;

		case 7:
			System.out.print("you can build in the southwestern");
			break;
		}

		// получаем нужное направление для строительства, если оно НЕ единственное
		switch (counter2) {
		case 0:
			System.out.print(" and in the northern");
			break;

		case 1:
			System.out.print(" and in the south");
			break;

		case 2:
			System.out.print(" and in the west");
			break;

		case 3:
			System.out.print(" and in the east");
			break;

		case 4:
			System.out.print(" and in the northwestern");
			break;

		case 5:
			System.out.print(" and in the northeastern");
			break;

		case 6:
			System.out.print(" and in the southeastern");
			break;

		case 7:
			System.out.print(" and in the southwestern");
			break;
		}

	}
}
