package HomeWork18;

public class binarySerch {

    public static void main(String[] args) {
        int[] data = new int[] { 0, 2, 3, 5, 8, 10, 15, 19, 25, 55, 75, 101, 122, 155, 190, 193 };
        int element = 5;

        int startLabel = 0;
        int endLabel = data.length;
        int len = endLabel - startLabel;
        int index = len / 2;

        System.out.println(startSearch(data, element, startLabel, endLabel, len, index));
    }

    /**
     * @return найден - true
     * @return не найден - false
     */
    public static boolean startSearch(int[] data, int element, int startLabel, int endLabel, int len, int index) {
        if (endLabel - startLabel == 1)
            if (data[startLabel] != element && data[endLabel] != element)
                return false;
            else
                return true;
        if (data[index] == element)
            return true;
        if (data[index] > element) {
            endLabel = index;
            len = endLabel - startLabel;
            index -= len / 2;
            return startSearch(data, element, startLabel, endLabel, len, index);
        }
        if (data[index] < element) {
            startLabel = index;
            len = endLabel - startLabel;
            index += len / 2;
            return startSearch(data, element, startLabel, endLabel, len, index);
        }
        return true;
    }
}
