package HomeWork2;

public class JavaDataType {

    public static void main(String[] args) {
        // ��������� ���������� � ����������� �� ��������

        byte selectedItem = 127;
        short smallNumber = 45;
        int number = 5456;
        long balance = 54787548L;
        float diameter = 3.45f;
        double radius = 4.89;
        boolean isDigit = true;
        char letter = 'x';

        // ������� �� �����

        System.out.println("\n\n*********VALUE********\n");
        System.out.println(selectedItem);
        System.out.println(smallNumber);
        System.out.println(number);
        System.out.println(balance);
        System.out.println(diameter);
        System.out.println(radius);
        System.out.println(isDigit);
        System.out.println(letter);

        // ������ ��������

        selectedItem = 90;
        smallNumber = 3;
        number = 653;
        balance = 1L;
        diameter = 90.12f;
        radius = 137.52;
        isDigit = false;
        letter = 'A';

        // ������� �� ����� ��������

        System.out.println("\n\n*******NEW VALUE******\n");

        System.out.println(selectedItem);
        System.out.println(smallNumber);
        System.out.println(number);
        System.out.println(balance);
        System.out.println(diameter);
        System.out.println(radius);
        System.out.println(isDigit);
        System.out.println(letter);
    }

}
