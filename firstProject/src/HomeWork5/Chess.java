package HomeWork5;

public class Chess {

	public static void main(String[] args) {
		int gorizontalSize = 8; // ����������� �����
		int verticalSize = 8; // ��������� �����

		int a = 8;// ���������� �������
		int b = 7;// ���������� �������
		int c = 7;// ���������� �������
		int d = 6;// ���������� �������

		displayChess(a, b, c, d, gorizontalSize, verticalSize);// ������ ������
		dangerRook(a, b, c, d);
		dangerHorse(a, b, c, d);
		dangerElephant(a, b, c, d);
		dangerKing(a, b, c, d);
		dangerQueen(a,b,c,d);
		dangerPawnA(a,b,c,d);
		dangerPawnB(a,b,c,d);
		dangerBlackPawn(a,b,c,d);
	}
	
	// ������ ��������� ����� � �������
	static void displayChess(int aa, int bb, int cc, int dd, int gorizontalSize, int verticalSize) {
		if (aa == cc && bb == dd) // �������� �� ���� � �� �� ������
			System.out.println("������ �� ����� ������!");
		else {
			for (int j = 1; j <= gorizontalSize; j++) {
				System.out.print("\n");
				for (int i = 1; i <= verticalSize; i++) {
					if (i == aa && j == bb) {
						System.out.print(" F "); // �������� "������"
					} else if (i == cc && j == dd) {
						System.out.print(" � ");// �������� "����"
					} else
						System.out.print(" * ");
				}
			}
		}
	}

	// �����
	static void dangerRook(int aa, int bb, int cc, int dd) {
		if (aa == cc || bb == dd) // ������� ��� �������� �� ������ �����
			System.out.println("\n����� �������� ������ " + cc + "x" + dd);
		else
			System.out.println("\n����� �� �������� ������ " + cc + "x" + dd);
	}

	// ����
	static void dangerHorse(int aa, int bb, int cc, int dd) {
		if (((aa + 2 == cc && bb + 1 == dd) || (bb + 2 == dd && aa + 1 == cc)) // ������� ��� �������� �� ������ ����
				|| ((aa + 2 == cc && bb - 1 == dd) || (bb + 2 == dd && aa - 1 == cc))
				|| ((aa - 2 == cc && bb + 1 == dd) || (bb - 2 == dd && aa + 1 == cc))
				|| ((aa - 2 == cc && bb - 1 == dd) || (bb - 2 == dd && aa - 1 == cc)))
			System.out.println("\n���� �������� ������ " + cc + "x" + dd);
		else
			System.out.println("\n���� �� �������� ������ " + cc + "x" + dd);
	}

	// ����
	static void dangerElephant(int aa, int bb, int cc, int dd) {
		boolean status = false;

		for (int i = 1; i <= 8; i++) {
			if (((aa == cc - i && bb == dd - i) || (aa == cc + i && bb == dd + i))
					|| ((aa == cc - i && bb == dd + i) || (aa == cc + i && bb == dd - i)))// ������� ��� �������� �� ������ c����
				status = true;
		}
		
		if (status)
			System.out.println("\n���� �������� ������ " + cc + "x" + dd);
		else
			System.out.println("\n���� �� �������� ������ " + cc + "x" + dd);
	}

	// ������
	static void dangerKing(int aa, int bb, int cc, int dd) {
		if ((aa == cc - 1 & bb == dd - 1) || (aa == cc & bb == dd - 1) || (aa == cc + 1 & bb == dd - 1) || // ������� ��� �������� �� ������ ������
				(aa == cc - 1 & bb == dd) || (aa == cc + 1 & bb == dd) || (aa == cc - 1 & bb == dd + 1)
				|| (aa == cc & bb == dd + 1) || (aa == cc + 1 & bb == dd + 1))
			System.out.println("\n������ �������� ������ " + cc + "x" + dd);
		else
			System.out.println("\n������ �� �������� ������ " + cc + "x" + dd);
	}
	
	// �����
	static void dangerQueen(int aa, int bb, int cc, int dd) {
		boolean status = false;
		for (int i = 1; i <= 8; i++) {
			if (((aa == cc - i && bb == dd - i) || (aa == cc + i && bb == dd + i))
					|| ((aa == cc - i && bb == dd + i) || (aa == cc + i && bb == dd - i)))// ������� ��� �������� �� ������ ����� ��� ����� 
				status = true;
		}
		if (aa == cc || bb == dd||status)
			System.out.println("\n����� �������� ������ " + cc + "x" + dd);
		else
			System.out.println("\n����� �� �������� ������ " + cc + "x" + dd);
	}
	
	// ����� ������� �
	static void dangerPawnA(int aa, int bb, int cc, int dd) {
		
			if ((aa==cc&&bb==dd+1)|| ((bb==7)&&aa==cc&&bb==dd+2)) // ������� ��� �������� �� ��� ����� 
				System.out.println("\n����� ����� ������� �� ���� CD " + cc + "x" + dd);
				else
					System.out.println("\n����� �� ����� ������� �� ���� CD " + cc + "x" + dd);
	
	}
	
	// ����� ������� �
	static void dangerPawnB(int aa, int bb, int cc, int dd) {
		
			if (((aa==cc-1||aa==cc+1)&&bb==dd+1))// ������� ��� �������� �� ��� ����� 
				System.out.println("\n����� �������� ���� CD " + cc + "x" + dd);
				else
					System.out.println("\n����� �� �������� ���� CD " + cc + "x" + dd);
	
	}
	
	// ����� ������
		static void dangerBlackPawn(int aa, int bb, int cc, int dd) {
			
				if ((aa==cc&&bb==dd-1)|| ((bb==2)&&aa==cc&&bb==dd-2)) // ������� ��� �������� �� ��� ����� 
					System.out.println("\n����� ����� ������� �� ���� CD " + cc + "x" + dd);
					else
						System.out.println("\n����� �� ����� ������� �� ���� CD " + cc + "x" + dd);
		
		}
}
